<?php

namespace Storage;

use Concept\Distinguishable;

class SessionStorage implements Storage
{
    public function __construct()
    {
        // TODO: ...
        session_start();
        if(!isset($_SESSION['object']))
        {
            $_SESSION['object']=[];
        }
    }

    public function store(Distinguishable $distinguishable) : void
    {
        // TODO: ...
        $key=$distinguishable->key();
        $data=serialize($distinguishable);
        array_push($_SESSION['object'],['key'=>$key, 'data'=>$data]);

    }

    public function loadAll(): array
    {
        // TODO: ...
        $result=[];
        foreach ($_SESSION['object'] as $file){
            if($file['data']=='.gitignore' || $file['data']=='.' || $file['data']=='..'  || $file['data']=='db.sqlite') {
                continue;
            }
            $result[]=unserialize($file['data']);
        }
        return $result;
    }
}