<?php

namespace Storage;

use Concept\Distinguishable;
use PDO;
use Config\Directory;

class Help_Storage implements Storage
{
    // TODO: ...
    public PDO $test;
    public function __construct()
    {
        // TODO: ...
        $this->test->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->test->exec("CREATE TABLE IF NOT EXISTS objects (`key` CHAR(15) NOT NULL PRIMARY KEY, `data` TEXT NOT NULL)");
        $this->test->exec("DELETE FROM objects");
    }

    public function store(Distinguishable $distinguishable) : void
    {
        // TODO: ...
        $statement = $this->test->prepare("INSERT INTO objects VALUES (:key, :data)");
        $statement->bindValue('key',$distinguishable->key());
        $statement->bindValue('data',serialize($distinguishable));
        $statement->execute();
    }

    public function loadAll(): array
    {
        // TODO: ...
        $query = $this->test->query("SELECT * FROM objects");
        $selected = $query->fetchAll();
        $results = [];
        foreach ($selected as $file) {
            if($file['data']=='.gitignore' || $file['data']=='.' || $file['data']=='..'  || $file['data']=='db.sqlite') {
                continue;
            }
            $results[] = unserialize($file['data']);
        }
        return $results;
    }

}