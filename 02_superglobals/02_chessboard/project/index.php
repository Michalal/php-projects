<?php
error_reporting(-1);
ini_set("display_errors", "On");
session_start();
error_reporting(0);
?>
<html lang="en">
<head>
    <title>Superglobals</title>

    <style type="text/css">
        .block {
            display: inline-block;
            width: 30px;
            height: 30px;
            padding: 0;
            margin: 0;
        }

        .block:hover {
            background-color: lightgray;
        }

        .red {
            background-color: red;
        }

        .blue {
            background-color: blue;
        }

        .green {
            background-color: green;
        }

        .gray {
            background-color: gray;
        }

        .white {
            background-color: #ffffff;
        }
    </style>
</head>
<body>
<div>
    <?php

    if(isset($_COOKIE['color']))
    {
        $color=$_COOKIE['color'];
    }
    else
    {
        $color='gray';
    }

    if(isset($_POST['color']))
    {
        setcookie('color', $_POST['color']);
        $color=$_POST['color'];
    }

    if(isset($_POST['sx']))
    {
        $wier=$_POST['sx'];
        $_SESSION['sx']=$wier;
    }
    elseif(isset($_SESSION['sx']))
    {
        $wier=$_SESSION['sx'];
    }
    else
    {
        $wier=10;
    }

    if(isset($_POST['sz']))
    {
        $kol=$_POST['sz'];
        $_SESSION['sz']=$kol;
    }
    elseif(isset($_SESSION['sz']))
    {
        $kol=$_SESSION['sz'];
    }
    else
    {
        $kol=10;
    }

    if(!isset($_SESSION['Array']))
    {
        for($i=0; $i<$kol; $i++)
        {
            for($j=0; $j<$wier; $j++)
            {
                $_SESSION['Array'][$j][$i]=0;
            }
        }
    }
    else
    {
        for($i=0; $i<$kol; $i++)
        {
            for($j=0; $j<$wier; $j++)
            {
                if($_SESSION['Array'][$j][$i]==0 || !isset($_SESSION['Array'][$j][$i]))
                {
                    $_SESSION['Array'][$j][$i]=0;
                }
                else
                {
                    $_SESSION['Array'][$j][$i]=1;
                }
            }
        }
    }


    function draw($pierw, $drug, $trzec, $czw, $piat, $szost, $siodm, $osmy)
    {
        $ai=($pierw-$drug)*2;
        $bi=$pierw*2;
        $d=$bi-$drug;
        while ($trzec!=$siodm)
        {
            if($d>=0)
            {
                $trzec+=$piat;
                $czw+=$szost;
                $d+=$ai;
            }
            else
            {
                $d+=$bi;
                $trzec+=$piat;
            }
            if($osmy==0)
                $_SESSION['Array'][$trzec][$czw]=1;
            else
                $_SESSION['Array'][$czw][$trzec]=1;
        }
    }

    function bresenham($x1, $y1, $x2, $y2)//na podstawie wikipedii
    {
        $x=$x1;
        $y=$y1;
        if($x1<$x2)
        {
            $xi=1;
            $dx=$x2-$x1;
        }
        else
        {
            $xi=-1;
            $dx=$x1-$x2;
        }
        if($y1<$y2)
        {
            $yi=1;
            $dy=$y2-$y1;
        }
        else
        {
            $yi=-1;
            $dy=$y1-$y2;
        }
        $_SESSION['Array'][$x][$y]=1;
        if($dx>$dy)
        {
           draw($dy,$dx,$x,$y,$xi,$yi,$x2,0);
        }
        else
        {
            draw($dx,$dy,$y,$x,$yi,$xi,$y2,1);
        }
    }


    if(!isset($_SESSION['flag']))
    {
        $_SESSION['flag']=0;
    }
    $boolean=0;
    
    if(isset($_GET['x']))
    {
        if($_SESSION['flag']==0)
        {
            $_SESSION['x1']=$_GET['x'];
        }
        elseif($_SESSION['flag']==1)
        {
            $_SESSION['x2']=$_GET['x'];
        }
    }
    
    if(isset($_GET['z']))
    {
        if($_SESSION['flag']==0)
        {
            $_SESSION['y1']=$_GET['z'];
            $_SESSION['flag']=1;
        }
        elseif($_SESSION['flag']==1)
        {
            $_SESSION['y2']=$_GET['z'];
            $_SESSION['flag']=0;
            $boolean=1;
        }
    }
    
    if($boolean==1)
    {
        $x1=$_SESSION['x1'];
        $x2=$_SESSION['x2'];
        $y1=$_SESSION['y1'];
        $y2=$_SESSION['y2'];
        //echo "SPRAWDZANIE x1: $x1,$y1     x2: $x2,$y2\n";
        bresenham($x1, $y1, $x2, $y2);
        $boolean=0;
    }

    for($i=0;$i<$kol;$i++)
    {
        echo "<div>";
        for($j=0;$j<$wier;$j++)
        {
            if($_SESSION['Array'][$j][$i]==0)
            {
                echo "<a class='block $color' href='?x=$j&z=$i'></a>";
            }
            else
            {
                echo "<a class='block white' href='?x=$j&z=$i'></a>";
            }
        }
        echo "</div>";
    }
    ?>
</div>
<br/>

<form method="post">
    <label>
        Columns:
        <input type="text" name="sx">
    </label>
    <label>
        Rows:
        <input type="text" name="sz">
    </label>
    <input type="submit" value="Change">
</form>

<form method="post">
    <label>
        Color:
        <select name="color">
            <option value="gray">Gray</option>
            <option value="red">Red</option>
            <option value="green">Green</option>
            <option value="blue">Blue</option>
        </select>
    </label>
    <input type="submit" value="Change">
</form>




</body>
</html>
