<?php
include "menu.php";
$example_users = [
    1 => [
        'name' => 'John',
        'surname' => 'Doe',
        'age' => 21
    ],
    2 => [
        'name' => 'Peter',
        'surname' => 'Bauer',
        'age' => 16
    ],
    3 => [
        'name' => 'Paul',
        'surname' => 'Smith',
        'age' => 18
    ]
];
$page = $_SERVER['REQUEST_URI'];
$page = substr($page,6,1);
//echo $page;
$user = $example_users[$page];
echo '<p>User:</p>';?>
    <p><strong>Name:</strong> <?= $user['name']?></p>
    <p><strong>Surname:</strong> <?= $user['surname']?></p>
    <p><strong>Age:</strong> <?= $user['age']?></p>
