<?php
    //echo $_SERVER['REQUEST_URI'];
    $page = $_SERVER['REQUEST_URI'];
    include "../views/layout.php";
    include "../views/main.php";
    if($page =='/' || $page == '/home')
    {
        include "../views/home.php";
    }
    elseif($page == '/about')
    {
        include "../views/about.php";
    }
    elseif($page == '/users')
    {
        include "../views/users.php";
    }
    elseif(substr($page,0,6) == '/user/' && ($page[6]==1 || $page[6]==2 || $page[6]==3))
    {
        include "../views/user.php";
    }
    else
    {
        include "../views/404.php";
    }

?>
<html lang="en">
<head>
    <title>Pretty URL</title>

    <style type="text/css">

    </style>
</head>

</html>