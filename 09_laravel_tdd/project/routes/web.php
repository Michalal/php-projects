<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/comments', App\Http\Controllers\CommentController::class);

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/books',
    [App\Http\Controllers\BookController::class, 'index'])->middleware(['auth'])->name('books.index');
Route::any('/books/create',
    [App\Http\Controllers\BookController::class, 'create']);
Route::get('/books/{book}',
    [App\Http\Controllers\BookController::class, 'show']);
Route::any('/books/{book}/edit',
    [App\Http\Controllers\BookController::class, 'edit']);
Route::put('/books/{book}',
    [App\Http\Controllers\BookController::class, 'update']);
Route::any('/books/{book}/delete',
    [App\Http\Controllers\BookController::class, 'delete']);
require __DIR__.'/auth.php';
