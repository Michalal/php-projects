<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Book;

class BookController extends Controller
{
    public function index()
    {
        return view('books.index', ['book'=>Book::all()]);
    }

    public function create()
    {
        if(request()->has('isbn'))
        {
            $book=Book::create($this->checkBook());
            return redirect('/books/'.$book->id);
        }
        return view('books.create');
    }

    public function show(Book $book)
    {
        return view('books.show', ['book'=>$book]);
    }

    public function edit(Book $book)
    {
        return view('books.edit', ['book'=>$book]);
    }

    public function update(Book $book)
    {
        $book->update($this->checkBook());
        return redirect('/books/'.$book->id);
    }

    public function checkBook()
    {
        return request()->validate(['isbn'=>['required','digits:13'],'title'=>'required','description'=>'required']);
    }

    public function delete(Book $book)
    {
        $book->delete();
        return redirect('/books');
    }
}
