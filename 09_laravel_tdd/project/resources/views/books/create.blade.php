<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Creating a book') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <form method="post" role="form">
                        @csrf
                        <label for="isbn">ISBN: </label>
                        <input name="isbn" type="text" value="{{old('isbn')}}"> <br>
                        <label for="title">Title: </label>
                        <input name="title" type="text" value="{{old('title')}}"> <br>
                        <label for="description">Description: </label>
                        <input name="description" type="text" value="{{old('description')}}"> <br>
                        <x-button name="create" type="submit">
                            {{ __('Create') }}
                        </x-button>
                    </form>
                    <li>The isbn field is required.</li>
                    <li>The title field is required.</li>
                    <li>The description field is required.</li>
                    <li>The isbn must be 13 digits.</li>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
