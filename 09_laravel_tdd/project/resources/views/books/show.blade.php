<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Viewing a book') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    {{$book->isbn}}
                    {{$book->title }}
                    <strong> {{$book->description}}</strong> <br>
                    <form method="post">
                        @csrf
                        <button type="submit" formaction="/books/{{$book->id}}/edit" name="Edit">Edit</button>
                        <button type="submit" formaction="/books/{{$book->id}}/delete"name="Delete">Delete</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
