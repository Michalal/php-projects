<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('List of books') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <p>No books in database.</p>
                    <a href="/books/create">Create new...</a>
                    <?php
                        foreach($book as $number)
                        {
                            echo "<tr><td>$number->isbn $number->title<a href=\""."/books/".$number->id."\" name="."Details".">Details</a></td></tr>";
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
