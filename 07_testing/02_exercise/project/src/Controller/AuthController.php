<?php

namespace Controller;

use Model\User;
use Storage\MySQLStorage;
use Storage\StorageFactory;
use Concept\Distinguishable;
class AuthController extends Controller
{
    // TODO: ...
    private ?MySQLStorage $sql = NULL;
    public function register()
    {
        return ["auth.register", ["title" => "Register"]];
    }
    public function login()
    {
        return ["auth.login", ["title" => "Login"]];
    }
    public function store_user()
    {

        if($this->sql == NULL)
        {
            $this->sql = new MySQLStorage();
        }

        $user = new User($_SESSION['id']);
        $user->name = $_SESSION['name'];
        $user->surname = $_SESSION['surname'];
        $user->email = $_SESSION['email'];
        $user->password = password_hash($_SESSION['password'], PASSWORD_DEFAULT);
        $user->confirmed_password = false;
        $user->token = bin2hex(random_bytes(16));
        $this->sql ->store($user);

    }
}