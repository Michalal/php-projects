<?php

namespace Container;

class RingBuffer
{
    // TODO: ...
    private $values;
    private int $begin;
    private int $end;
    private int $current_size;
    private int $max_size;
    public function __construct(int $_size)
    {
        $this->values=array();
        $this->max_size=$_size;
        $this->begin=-1;
        $this->end=-1;
        $this->current_size=0;
    }
    public function empty()
    {
        if($this->current_size==0)
            return true;
        return false;
    }

    public function capacity()
    {
        return $this->max_size;
    }

    public function size()
    {
        return $this->current_size;
    }

    public function full()
    {
        if($this->current_size==$this->max_size)
            return true;
        return false;
    }

    public function pop()
    {
        if($this->end==-1)
            return null;
        $zmienna=$this->values[$this->begin];
        if($this->current_size>0)
            $this->current_size=$this->current_size-1;
        $this->values[$this->begin]=null;
        $this->begin=($this->begin+1)%$this->max_size;
        return $zmienna;
    }

    public function push($int)
    {
        if($this->current_size<$this->max_size)
            $this->current_size=$this->current_size+1;
        $this->end=($this->end+1)%$this->max_size;
        $this->values[$this->end]=$int;
        if($this->begin==-1)
            $this->begin=0;
        else if($this->end==$this->begin)
            $this->begin=($this->begin+1)%$this->max_size;
    }

    public function tail()
    {
        if($this->values==0)
        {
            return null;
        }
        return $this->values[$this->begin];
    }

    public function head()
    {
        if($this->values==0)
        {
            return null;
        }
        return $this->values[$this->end];
    }

    public function at(int $int)
    {
        if($int>=0 && $int<$this->size())
        {
            return $this->values[$int];
        }
        return null;
    }
}