<?php

namespace String;

class Editor
{
    // TODO: ...
    private $editor;
    public function __construct($text)
    {
        $this->editor=$text;
    }
    public function get()
    {
        return $this->editor;
    }

    public function replace(string $string, string $string1)
    {
        $change=$this->editor;
        $this->editor=str_replace($string,$string1,$change);
        return $this;
    }

    public function lower()
    {
        $change=strtolower($this->editor);
        $this->editor=$change;
        return $this;
    }

    public function upper()
    {
        $change=strtoupper($this->editor);
        $this->editor=$change;
        return $this;
    }

    public function censor(string $string)
    {
        $s=NULL;
        $len=0;
        while($len<strlen($string))
        {
            $s=$s."*";
            $len=$len+1;
        }
        return $this->replace($string,$s);
    }

    public function repeat(string $string, int $int)
    {
        $s=NULL;
        $len=0;
        while($len<$int)
        {
            $s=$s.$string;
            $len=$len+1;
        }
        return $this->replace($string,$s);
    }

    public function remove(string $string)
    {
        return $this->replace($string,"");
    }
}
