<?php

$array = ['id', 'name', 'surname', 'email', 'password', 'password_confirmation'];
if(isset($_POST['create']))
{
    $error=false;
    foreach ($array as $element)
    {
        if(empty($_POST[$element]))
        {
            echo(str_replace('_', ' ', '<li class="error"> The ' . $element . ' filed cannot be empty</li>'));
            $error=true;
        }
        else
            $_SESSION[$element]=$_POST[$element];

    }
    if(!$error && $_POST['password']!=$_POST['password_confirmation'])
    {
        echo('<li class="error">The password confirmation filed does not match the password field</li>');
        $error=true;
    }
}
else
{
    foreach ($array as $element)
    {
        $_SESSION[$element]=NULL;
    }
}

?>

<h2>Register</h2>
<form method="post">

    <p><input id="id" name="id" type="text" value=<?php echo($_SESSION['id'])?>><br></p>
    <p><input id="name" name="name" type="text" value=<?php echo($_SESSION['name'])?>><br></p>
    <p><input id="surname" name="surname" type="text" value=<?php echo($_SESSION['surname'])?>><br></p>
    <p><input id="email" name="email" type="text" value=<?php echo($_SESSION['email'])?>><br></p>
    <p><input id="password" name="password" type="password" value=""><br></p>
    <p><input id="password_confirmation" name="password_confirmation" type="password" value=""><br></p>
    <p><input name="create" type="submit" value="Create"><br></p>
</form>



