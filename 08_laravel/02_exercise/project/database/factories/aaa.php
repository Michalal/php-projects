<?php


namespace Database\Factories;
use App\Models\Comments;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class CommentsFactory extends Factory
{
    protected $model=Comments::class;
    public function definition()
    {
        // TODO: Implement definition() method.
        return ['title'=>$this->faker->title, 'email'=>$this->faker->text];
    }

}
