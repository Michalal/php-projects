<?php

namespace Storage;

use Concept\Distinguishable;
// TODO: ...
use Redis;
class RedisStorage implements Storage
{
    // TODO: ...
    private Redis $red;
    public function __construct()
    {
        // TODO: ...
        $this->red=new Redis();
        $this->red->connect('127.0.0.1',6379);
    }

    public function store(Distinguishable $distinguishable) : void
    {
        // TODO: ...
        $this->red->set($distinguishable->key(),serialize($distinguishable));
    }

    public function loadAll(): array
    {
        $list = $this->red->keys("*");
        $result = [];
        foreach ($list as $key)
        {
            $result[] = unserialize($this->red->get($key));
        }
        return $result; // TODO: ...
    }
}