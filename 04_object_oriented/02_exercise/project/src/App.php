<?php
use Storage\FileStorage, Widget\Button, Widget\Link, Widget\Widget;
class App
{
    public function run()
    {
        $widgets = [new Link(1), new Link(2), new Link(3), new Button(1), new Button(2), new Button(3)];
        $storage = new FileStorage();
        foreach ($widgets as $widget)
        {
            $storage->store($widget);
        }
        $widgets = $storage ->loadAll();
        foreach ($widgets as $widget)
        {
            $this->render($widget);
        }
    }
    private function render(Widget $widget)
    {
        $widget->draw();
    }
}
