<?php

namespace Concept;
use Storage\FileStorage;
abstract class Distinguishable
{
    private $id;
    public function __construct($id)
    {
        $this->id=$id;
    }
    public function key()
    {
        return $this->normalize(static::class).'_'.$this->id;
    }
    private function normalize($string)
    {
        $string[0]='w';
        if($string[7]=='L')
        {
            $string[7]='l';
        }
        else
        {
            $string[7]='b';
        }
        $second=explode('\\',$string);
        return $second[0].'_'.$second[1];
    }
}