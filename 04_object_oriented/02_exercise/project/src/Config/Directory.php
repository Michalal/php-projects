<?php

namespace Config;
class Directory
{
    private static $root;
    public static function set($root)
    {
       	// TODO: ...
        Directory::$root = $root;
    }
    public static function root():string
    {
        return Directory::$root;
    }
    public static function storage():string
    {
        return Directory::$root.'storage/';
    }
    public static function view():string
    {
        return Directory::$root.'public/';
    }
    public static function src():string
    {
        return Directory::$root.'src/';
    }
}
