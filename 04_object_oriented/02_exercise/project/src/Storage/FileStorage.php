<?php

namespace Storage;
use Concept\Distinguishable, Config\Directory;
class FileStorage
{
    public function store(Distinguishable $d)
    {
        $data=serialize($d);
        $file_path=Directory::storage().$d->key();
        file_put_contents("$file_path", "$data");
    }
    public function loadAll()
    {
        $storage=scandir(Directory::storage());
        $Deserialized_Array=array();
        foreach ($storage as $check)
        {
            if($check=='.gitignore' || $check=='.' || $check=='..')
            {
                continue;
            }
            $result=unserialize(file_get_contents(Directory::storage().$check));
            array_push($Deserialized_Array,$result);
        }
        return $Deserialized_Array;
    }
}
