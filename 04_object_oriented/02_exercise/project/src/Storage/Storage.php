<?php


interface Storage extends FileStorage
{
    public function store(Distinguishable $d);
    public function loadAll();
}